package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream().anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));
        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        HolyWish holyWish2 = HolyWish.getInstance();
        assertEquals(holyWish, holyWish2);
    }

    @Test
    public void testWish(){

        holyWish.setWish("I want to play apex");
        assertEquals("I want to play apex", holyWish.getWish());
    }

    @Test
    public void testWishExist(){

        HolyWish holyWish2 = HolyWish.getInstance();

        // jika wish 1 di set, seharusnya wish 2 juga di set karena singleton
        holyWish.setWish("I want to sleep");
        assertNotNull(holyWish2);
        assertEquals("I want to sleep", holyWish2.getWish());

    }

}
