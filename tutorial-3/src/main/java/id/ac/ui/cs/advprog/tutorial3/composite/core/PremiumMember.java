package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String name, role;
    private List<Member> memberList;

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
        this.memberList = new ArrayList<>();
    }

    public String getName(){
        return this.name;
    }

    public String getRole(){
        return this.role;
    }

    public void addChildMember(Member member){
        if(memberList.size() < 3 || this.role.equals("Master"))
            this.memberList.add(member);
    }

    public void removeChildMember(Member member){
        memberList.remove(member);
    }

    public List<Member> getChildMembers(){
        return this.memberList;
    }
}
