package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.HolyGrail;
@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    HolyGrail holyGrail;
    @BeforeEach
    public void setUp() throws Exception {

        holyGrail = new HolyGrail();
        holyGrail.makeAWish("i want to play apex");
    }

    @Test
    public void testSetWish() {
        holyGrail.makeAWish("i want to play as wattson");
        assertEquals("i want to play as wattson", holyGrail.getHolyWish().getWish());
    }

    @Test
    public void testGetWish() {
        assertEquals("i want to play apex", holyGrail.getHolyWish().getWish());
    }


}
